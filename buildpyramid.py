#the purpose of this file is to build a 3-2-1 pyramid
#it uses some hardcoded values to be able to work consistently
from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
import math

from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list
from gazebo_msgs.srv import SpawnModel, DeleteModel

import actionlib
from franka_gripper.msg import GraspAction, GraspGoal

#initialize robot and scene
moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node("panda", anonymous=True)
robot = moveit_commander.RobotCommander()
scene = moveit_commander.PlanningSceneInterface()

#create move_group
group_name = "panda_arm"
move_group = moveit_commander.MoveGroupCommander(group_name)

#greate gripper move group
hand_group_name = "panda_hand"
move_group_hand = moveit_commander.MoveGroupCommander(hand_group_name)

display_trajectory_publisher = rospy.Publisher(
    "/move_group/display_planned_path",
    moveit_msgs.msg.DisplayTrajectory,
    queue_size=20,
)

#allows robot to move to specified x and y coordinates
def move_to_posn(x, y):
    waypoints = []
    current_pose = move_group.get_current_pose().pose
    current_pose.position.x = x
    current_pose.position.y = y
    waypoints.append(copy.deepcopy(current_pose))
    (plan, fraction) = move_group.compute_cartesian_path(
    waypoints, 0.01, 0.0)
    move_group.execute(plan, wait=True)

#allows robot to move to a specified z coordinate
def move_to_z(z):
    waypoints = []
    current_pose = move_group.get_current_pose().pose
    current_pose.position.z = z
    waypoints.append(copy.deepcopy(current_pose))
    (plan, fraction) = move_group.compute_cartesian_path(
    waypoints, 0.01, 0.0)
    move_group.execute(plan, wait=True)

#open the gripper to its maximum width
def open_gripper():
    move_group_hand.go([.04, .04])
    move_group_hand.stop()

#close gripper around cup
#from https://github.com/frankaemika/franka_ros/issues/125 
def grip_cup():
    client = actionlib.SimpleActionClient('franka_gripper/grasp', GraspAction)
    client.wait_for_server()
    #set graspgoal with specified parametrs
    goal = GraspGoal()
    goal.width = .06
    goal.speed = .03
    goal.force = 10
    goal.epsilon.inner = 0.005
    goal.epsilon.outer = 0.002
    client.send_goal(goal)
    wait = client.wait_for_result()
    if not wait:
        rospy.logerr("Action server not available!")
        rospy.signal_shutdown("Action server not available!")
    else:
        return client.get_result()

#starting joint values to get end effector in correct orientation
initial_joints = [-0.00017287882748195926, -0.7853434914126236, -4.2369772542016904e-05, -2.356230316475253, 0.00011566587457956246, 3.1415, 0.785]
move_group.go(initial_joints)
move_group.stop()

from geometry_msgs.msg import Pose

#load spawning and deleting services
spawn_model_client = rospy.ServiceProxy('/gazebo/spawn_sdf_model', SpawnModel)
delete_model_client = rospy.ServiceProxy('/gazebo/delete_model', DeleteModel)

#spawn a cup in a specified x and y location with a specific number as its name
#from https://answers.ros.org/question/337065/what-is-the-correct-way-to-spawn-a-model-to-gazebo-using-a-python-script/
def spawn_cup(cup_number, x, y):
    spawn_model_client(
        model_name='cup'+ str(cup_number),
        model_xml=open('./cup.sdf', 'r').read(),
        robot_namespace='/foo',
        initial_pose=Pose(position= geometry_msgs.msg.Point(x,y,.15),orientation=geometry_msgs.msg.Quaternion(1,0,0,0)),
        reference_frame='world'
    )

#delete a cup from gazebo
def delete_cup(cup_number):
    delete_model_client(
        model_name='cup'+str(cup_number),
    )

#delete all existing cups
delete_cup(1)
delete_cup(2)
delete_cup(3)
delete_cup(4)
delete_cup(5)
delete_cup(6)

#pick up cup from specified start x and y, move to correct location on pyramid and place the cup
def pick_up_cup(start_x, start_y, end_x, end_y, level):
    #move to location a distance behind the cup and slightly above
    move_to_posn(start_x-.2, start_y)
    move_to_z(.3)
    #joint values to make sure end effector is in proper orientation
    move_group.go([0.014759563594679115, -1.76277193835986, 1.6326559882373903, -2.3453668247225936, 0.2681399119273671, 2.3205716725465164, 2.22459002812019])
    move_group.stop()
    open_gripper()
    #move to the same z value as the cup
    move_to_z(.1)
    #move the gripper to be directly around the cup then grip it
    move_to_posn(start_x-.106, start_y)
    grip_cup()
    #move to a z high enough above the pyramid spot at the correct x and y
    move_to_z(.1+.1*(level+1))
    move_to_posn(end_x, end_y)
    #move to the z required to place the cup and open the gripper
    move_to_z(.1+level*.135)
    open_gripper()
    #move the end effector away
    move_to_posn(end_x-.1, end_y)
    move_to_z(.3)

#spawn each cup and place it at the correct location for a 3-2-1 pyramid
spawn_cup(1, .2, .5)
pick_up_cup(.2, .5, .34, .6, 0)

spawn_cup(2, .25, .5)

pick_up_cup(.25, .5, .26, .6, 0)

spawn_cup(3, .2, .5)

pick_up_cup(.2, .5, .18, .6, 0)

spawn_cup(4, .2, .5)

pick_up_cup(.2, .5, .31, .6, 1)

spawn_cup(5, .2, .5)
pick_up_cup(.2,.5, .22, .6, 1)

spawn_cup(6, .2, .5)
pick_up_cup(.2,.5, .26, .6, 2)

