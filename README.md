# ME442/ECE 383 Team 4: Final Project
# Morgan Feist, Nils Roede, Jordan Schwartz, Sophia Simms

This repository contains scripts for commanding a Franka Panda robot to perform cup stacking along with computer vision algorithms
to determine initial cup positions.

**get_panda_arm.txt** outlines the process followed to successfully load and command the Franka Panda arm within the MoveIt and Gazebo programs of our virtual machines. Our cup model, **cup.sdf**, has been modified from an existing Gazebo model. Changes include the red color (for easier identification by our computer vision system) and the addition of an invisible base such that cups can be stacked on a more stable, flat surface. 

We have demonstrated two methods of building the pyramid structure. The first attempt is in **buildpyramid.py** which builds a 3-2-1 pyramid relatively efficiently using hardcoded positions. 

In order to incorporate a variable pyramid size, we needed more constraints on end effector orientation so that the pyramid could be reliably built. Therefore, in **gripcup.py** we allow for a variable pyramid size, but the robot takes longer to build the pyramid.

The computer vision system is able to detect randomly spawned cups within the camera's field of view and publish coordinates to a ROS topic. This system has yet to be integrated with the rest of the project. 

