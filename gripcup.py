from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
import math

from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list
from gazebo_msgs.srv import SpawnModel, DeleteModel
import tf.transformations

import actionlib
from franka_gripper.msg import GraspAction, GraspGoal

from geometry_msgs.msg import Pose

# initialize robot and scene
moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node("panda", anonymous=True)
robot = moveit_commander.RobotCommander()
scene = moveit_commander.PlanningSceneInterface()

# create move_group
group_name = "panda_arm"
move_group = moveit_commander.MoveGroupCommander(group_name)

# create gripper move group
hand_group_name = "panda_hand"
move_group_hand = moveit_commander.MoveGroupCommander(hand_group_name)

# publisher to visualize planned trajectories
display_trajectory_publisher = rospy.Publisher(
    "/move_group/display_planned_path",
    moveit_msgs.msg.DisplayTrajectory,
    queue_size=20,
)

# Apply an RPY rotation to a pose in its parent coordinate system.
# from https://answers.ros.org/question/373104/quaternion-wrong-orientation/
def rotate_pose_by_rpy(in_pose, roll, pitch, yaw):
  try:
    if in_pose.header: # if in_pose is a PoseStamped instead of a Pose.
      in_pose.pose = rotate_pose_by_rpy(in_pose.pose, roll, pitch, yaw)
      return in_pose
  except:
    pass
  q_in = [in_pose.orientation.x, in_pose.orientation.y, in_pose.orientation.z, in_pose.orientation.w]
  q_rot = tf.transformations.quaternion_from_euler(roll, pitch, yaw)
  q_rotated = tf.transformations.quaternion_multiply(q_in, q_rot)

  rotated_pose = copy.deepcopy(in_pose)
  rotated_pose.orientation = geometry_msgs.msg.Quaternion(*q_rotated)
  return rotated_pose

# Move robot to specified x and y coordinates, constrained to specific orientation and maintaining z-coord
def move_to_posn(x, y):
    current_pose = move_group.get_current_pose().pose
    current_pose.position.x = x
    current_pose.position.y = y
    current_pose.orientation.x = 0
    current_pose.orientation.y = 0
    current_pose.orientation.z = 0
    current_pose.orientation.w = 1
    current_pose = rotate_pose_by_rpy(current_pose, 0, math.pi/2, 0)
    current_pose = rotate_pose_by_rpy(current_pose, 0, 0, math.pi/4)
    move_group.go(current_pose, wait=True)

# Move robot to specified z coordinate, maintaining x and y coords
def move_to_z(z):
    current_pose = move_group.get_current_pose().pose
    current_pose.position.z = z
    move_group.go(current_pose, wait=True)

# Open the gripper to its maximum width
def open_gripper():
    move_group_hand.go([.039, .039])
    move_group_hand.stop()

# Close gripper around cup
# from https://github.com/frankaemika/franka_ros/issues/125 
def grip_cup():
    client = actionlib.SimpleActionClient('franka_gripper/grasp', GraspAction)
    client.wait_for_server()
    # set grasp goal with specified paramaters
    goal = GraspGoal()
    goal.width = .06
    goal.speed = .03
    goal.force = 10
    goal.epsilon.inner = 0.005
    goal.epsilon.outer = 0.002
    client.send_goal(goal)
    wait = client.wait_for_result()
    if not wait:
        rospy.logerr("Action server not available!")
        rospy.signal_shutdown("Action server not available!")
    else:
        return client.get_result()

# Starting joint values for optimal mobility
initial_joints = [-0.00017287882748195926, -0.7853434914126236, -4.2369772542016904e-05, -2.356230316475253, 0.00011566587457956246, 3.1415, 0.785]
move_group.go(initial_joints)
move_group.stop()

# Load spawning and deleting services
spawn_model_client = rospy.ServiceProxy('/gazebo/spawn_sdf_model', SpawnModel)
delete_model_client = rospy.ServiceProxy('/gazebo/delete_model', DeleteModel)

# Spawn a cup in a specified x and y location with a specific number as its name
# from https://answers.ros.org/question/337065/what-is-the-correct-way-to-spawn-a-model-to-gazebo-using-a-python-script/
def spawn_cup(cup_number, x, y):
    spawn_model_client(
        model_name='cup'+ str(cup_number),
        model_xml=open('./cup.sdf', 'r').read(),
        robot_namespace='/foo',
        initial_pose=Pose(position= geometry_msgs.msg.Point(x,y,.15),orientation=geometry_msgs.msg.Quaternion(1,0,0,0)),
        reference_frame='world'
    )

# Delete a cup from gazebo
def delete_cup(cup_number):
    delete_model_client(
        model_name='cup'+str(cup_number),
    )

# Pick up cup from specified start x and y, move to correct location on pyramid and place the cup
def pick_up_cup(start_x, start_y, end_x, end_y, end_z):
    move_to_posn(start_x-.2, start_y)
    open_gripper()
    # move to the same z value as the cup
    move_to_z(.1)
    # move the gripper to be directly around the cup then grip it
    move_to_posn(start_x-.106, start_y)
    grip_cup()
    # move to a z high enough above the pyramid spot at the correct x and y
    move_to_z(end_z+.15)
    # move to the z required to place the cup and open the gripper
    move_to_posn(end_x, end_y)
    rospy.sleep(2)
    move_to_z(end_z)
    open_gripper()
    # move the end effector back and up
    move_to_posn(end_x-.1, end_y)
    move_to_z(end_z+.15)

# Modifiable width of pyramid base
stack_width = 4
pyramid = []

# Generate all poses for pyramid based on input width
for lvl in reversed(range(stack_width)):
    for i in range(lvl+1):
        pose_coords = (0.3 - i * .02 - (-1.35*lvl/2 + i) * .08, 0.6, .1+(stack_width - lvl - 1)*.135)
        pyramid.append(pose_coords)

print("Pyramid array = ", pyramid)

# Spawn each cup, then move it to correct position
for cup_num, coords in enumerate(pyramid):
    spawn_cup(cup_num, .195, 0.51)
    pick_up_cup(.2, .5, coords[0], coords[1], coords[2])

# Wait for 10 seconds
rospy.sleep(10)

# Clear all cups from Gazebo
for cup in range(len(pyramid)):
    delete_cup(cup)
