from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
import math

from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

from gazebo_msgs.srv import SpawnModel
from geometry_msgs.msg import Pose


from geometry_msgs.msg import Pose
spawn_model_client = rospy.ServiceProxy('/gazebo/spawn_sdf_model', SpawnModel)

def spawn_camera(cup_number, x, y, z):
    spawn_model_client(
        model_name='camera'+ str(cup_number),
        model_xml=open('./camera_model/model.sdf', 'r').read(),
        robot_namespace='/foo',
        initial_pose=Pose(position= geometry_msgs.msg.Point(x,y,z),orientation=geometry_msgs.msg.Quaternion(0,0.7071,0,0.7071)),
        reference_frame='world'
    )

spawn_camera(1, .7, .7, 1)

