from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
import math
from random import random

from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

from gazebo_msgs.srv import SpawnModel
from geometry_msgs.msg import Pose


from geometry_msgs.msg import Pose
spawn_model_client = rospy.ServiceProxy('/gazebo/spawn_sdf_model', SpawnModel)

def spawn_cup(cup_number, x, y):
    spawn_model_client(
        model_name='cup'+ str(cup_number),
        model_xml=open('../cup.sdf', 'r').read(),
        robot_namespace='/foo',
        initial_pose=Pose(position= geometry_msgs.msg.Point(x,y,.2),orientation=geometry_msgs.msg.Quaternion(1,0,0,0)),
        reference_frame='world'
    )
    
random_x = random() * (1.0 - 0.1) + 0.1
random_y = random() * (1.0 - 0.4) + 0.4
  
spawn_cup(1, random_x, random_y)

print(f"Spawn x: {random_x}, Spawn y: {random_y}")

