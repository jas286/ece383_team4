import rospy
from sensor_msgs.msg import Image
from geometry_msgs.msg import Vector3
import cv2
from cv_bridge import CvBridge
from math import tan

pub = None
WIDTH = 320
HEIGHT = 320
DIST_TO_FLOOR = 1
FOV = 1.047 # radians
CENTER_X = 0.7
CENTER_Y = 0.7


def callback(data):
    cv_image = CvBridge().imgmsg_to_cv2(data, "bgr8")
    centroid_image(cv_image)

    
def begin():
    global pub
    rospy.init_node('computer_vision', anonymous=True)
    rospy.Subscriber("foo/rrbot/camera1/image_raw", Image, callback)
    pub = rospy.Publisher("computer_vision/coordinate", Vector3, queue_size=10)
    rospy.spin()
    
    
def centroid_image(img):
    # Source: https://learnopencv.com/find-center-of-blob-centroid-using-opencv-cpp-python/
    # convert image to grayscale image
    gray_image = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # convert the grayscale image to binary image
    ret,thresh = cv2.threshold(gray_image,127,255,cv2.THRESH_BINARY_INV)
    thresh = cv2.inRange(img, (0, 0, 210), (255, 255, 255))

    # calculate moments of binary image
    M = cv2.moments(thresh)

    # calculate x,y coordinate of center
    cX = int(M["m10"] / M["m00"])
    cY = int(M["m01"] / M["m00"])
    
    x_frac_of_fov = -(cY - WIDTH/2) / (WIDTH/2)
    x_rad = x_frac_of_fov * FOV/2
    x_pos = tan(x_rad) * DIST_TO_FLOOR
    
    y_frac_of_fov = -(cX - WIDTH/2) / (WIDTH/2)
    y_rad = y_frac_of_fov * FOV/2
    y_pos = tan(y_rad) * DIST_TO_FLOOR
    
    x_pos += CENTER_X
    y_pos += CENTER_Y
    

    # put text and highlight the center
    cv2.circle(img, (cX, cY), 5, (255, 255, 255), -1)
    
    center = Vector3(x_pos, y_pos, 0)
    pub.publish(center)

    # display the image
    # cv2.imshow("Image", thresh)
    # cv2.waitKey(0)


if __name__ == '__main__':
    begin()
