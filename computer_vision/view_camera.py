import rospy
from sensor_msgs.msg import Image
import cv2
from cv_bridge import CvBridge


def callback(data):
    cv_image = CvBridge().imgmsg_to_cv2(data, "bgr8")
    cv2.imshow("Image window", cv_image)
    cv2.waitKey(3)

    
def listener():
    rospy.init_node('listener', anonymous=True)
    rospy.Subscriber("foo/rrbot/camera1/image_raw", Image, callback)
    rospy.spin()

if __name__ == '__main__':
    listener()
